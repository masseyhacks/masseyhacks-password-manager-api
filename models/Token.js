require('dotenv').config();
const mongoose = require('mongoose');

var schema = {
    token: {
        type: String,
        required: true
    },
    user: {
        type: String,
        required: true
    },
    createdAt: {
        type: Number,
        time: true,
        required: true
    },
    expiresAt: {
        type: Number,
        time: true,
        required: true
    }
};

var mongoSchema = new mongoose.Schema(schema);

module.exports = mongoose.model('Token', mongoSchema);