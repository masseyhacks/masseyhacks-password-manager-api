require('dotenv').config();
const mongoose = require('mongoose');

var grantSchema = {
    type: {
        type: String,
        required: true
    },
    id: {
        type: String,
        required: true
    }
};

var schema = {
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    username: {
        type: String
    },
    url: {
        type: String
    },
    password: {
        type: String,
        required: true
    },
    grants: {
        type: [grantSchema]
    }
};

var mongoSchema = new mongoose.Schema(schema);

module.exports = mongoose.model('Password', mongoSchema);