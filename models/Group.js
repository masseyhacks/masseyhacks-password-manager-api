require('dotenv').config();
const mongoose = require('mongoose');

var schema = {
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    members: {
        type: [String],
        required: true,
        default: []
    }
};

var mongoSchema = new mongoose.Schema(schema);

mongoSchema.statics.getByName = function (name, callback) {
    this.findOne({
        name: name
    }, function (err, group) {
        if (err || !group){
            return callback(err ? err : {
                message: 'Group not found',
                code: 400
            })
        }
        return callback(null, group);
    });
};

mongoSchema.statics.getByMember = function (id, callback) {
    this.find({
        members: id
    }, function (err, groups) {
        if (err || !groups){
            return callback(err ? err : {
                message: 'User does not belong in any groups',
                code: 200
            })
        }
        return callback(null, groups);
    });
};

module.exports = mongoose.model('Group', mongoSchema);