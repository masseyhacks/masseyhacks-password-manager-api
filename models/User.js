require('dotenv').config();
const mongoose = require('mongoose');

const UNVERIFIED_HACKER = 0;
const HACKER            = 1;
const CHECK_IN          = 2;
const ADMIN             = 3;
const REVIEW            = 4;
const OWNER             = 5;
const DEVELOPER         = 6;
const INTERNAL          = 99999;

var schema = {
    firstName: {
        type: String,
        required: true,
        maxlength: 50
    },

    lastName: {
        type: String,
        required: true,
        maxlength: 50
    },

    email: {
        type: String,
        required: true,
        maxlength: 50
    },

    password: {
        type: String,
        required: true,
        select: false,
        permission: INTERNAL
    },

    passwordLastUpdated: {
        type: Number,
        default: 0,
        time: true
    },

    permLevel: {
        type: Number,
        default: 0,
        required: true
    }
};

var mongoSchema = new mongoose.Schema(schema);

mongoSchema.statics.getByID = function (id, callback) {

    this.findOne({
        _id: id
    }, function (err, user) {
        if (err || !user) {
            return callback(err ? err : {
                error: 'User not found.',
                code: 404
            })

        }

        return callback(null, user);
    });
};

mongoSchema.statics.getByEmail = function (email, callback) {
    this.findOne({
        email: email ? email.toLowerCase() : email
    }, function (err, user) {
        if (err || !user) {
            return callback(err ? err : {
                error: 'User not found',
                code: 404
            })
        }
        return callback(null, user);
    });
};

mongoSchema.statics.filterSensitive = function (user, permission) {
    return filterSensitive(user, permission);
};

var filterSensitive = function (user, permission) {

    try {

        var u = user.toJSON();

        var permissionLevel;

        if (permission) {
            permissionLevel = permission;
        } else {
            permissionLevel = 0;
        }

        var queue = [[schema, u]];
        var runner;
        var userpath;
        var keys;

        console.log('Permission level:', permissionLevel);

        while (queue.length !== 0) {
            runner = queue[0][0];
            userpath = queue.shift()[1];
            keys = Object.keys(runner);

            for (var i = 0; i < keys.length; i++) {
                if ('type' in runner[keys[i]]) {
                    if (runner[keys[i]].permission && runner[keys[i]].permission > permissionLevel) {
                        try {
                            delete userpath[keys[i]];
                        } catch (e) {
                            console.log(e)
                        }
                    }
                    if (permissionLevel < 2 && runner[keys[i]].condition && !navigate(user, runner[keys[i]].condition)) {
                        userpath[keys[i]] = runner[keys[i]].default;
                    }

                } else {
                    if (userpath[keys[i]]) {
                        queue.push([runner[keys[i]], userpath[keys[i]]])
                    }
                }
            }
        }

        return u;
    } catch (e) {
        console.log("error");
        console.log(e)
        return {};
    }
};

module.exports = mongoose.model('User', mongoSchema);