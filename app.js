require('dotenv').config();
var express = require('express');
var logger = require('morgan');

const mongoose = require('mongoose');
const database = process.env.DATABASE || 'mongodb://localhost:27017';

var apiRouter = require('./routes/api');
var authRouter = require('./routes/auth');

var app = express();

console.log(database);
mongoose.connect(database, {
    useNewUrlParser: true,
    server: {
        auto_reconnect: true
    }
}
    )
    .then(() => {
        console.log("DB CONNECTED")
    })
    .catch(error => {
        console.log("DB CONNECTION ERROR");
        console.log(error)
    });

app.use(logger('dev'));
app.use(express.json());

app.use('/api', apiRouter);
app.use('/auth', authRouter);

module.exports = app;
