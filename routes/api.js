var express = require('express');
var router = express.Router();

const GroupController = require('../controllers/GroupController');
const PasswordController = require('../controllers/PasswordController');

const permissions = require('../services/permissions');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('api.js', { title: 'Express' });
});

router.post('/group/', permissions.isUser, function(req, res, next){
    console.log(req.body);
    if(!req.body.name) {
        return res.status(400).json({"error": "Incomplete data"})
    }
    GroupController.createGroup(req.body.name, req.body.description, function(err, message){
        if(err){
            return res.status(500).json(err)
        }
        return res.status(200).json(message)
    })
});

router.get('/group/:id', function(req, res, next){
    console.log(req.body);
    res.json({"message": "OK"})
});

router.put('/group/:id', function(req, res, next){
    console.log(req.body);
    res.json({"message": "OK"})
});

router.delete('/group/:id', function(req, res, next){
    console.log(req.body);
    res.json({"message": "OK"})
});

router.get('/group/:id/member', function(req, res, next){
  console.log(req.body);
  res.json({"message": "OK"})
});

router.put('/group/:id/member', function(req, res, next){
  console.log(req.body);
  if(!req.body.id){
    return res.status(400).json({"error": "Incomplete data"})
  }
  GroupController.addMember(req.body.id, req.params.id, function(err, group){
    if(err){
      return res.status(500).json(err)
    }
    return res.status(200).json(group)
  });
});

router.delete('/group/:id/member', function(req, res, next){
  console.log(req.body);
  res.json({"message": "OK"})
});

router.post('/password/', function(req, res, next){
    if(!req.body.password || !req.body.name){
        return res.status(400).json({"error": "Incomplete data"})
    }
    PasswordController.createPassword(req.body.name, req.body.password, req.body.username, req.body.description, req.body.url, function(err, message){
        if(err){
            return res.status(500).json(err);
        }
        return res.status(200).json(message);
    });
});

router.get('/password/:id', function(req, res, next){
    PasswordController.retrievePassword(req.params.id, function(err, password){
        if(err){
            return res.status(500).json(err);
        }
        return res.status(200).json(password)
    })
});

router.put('/password/:id', function(req, res, next){
    console.log(req.body);
    PasswordController.updatePassword(req.params.id, req.body.password, function(err, password){
        if(err){
            return res.status(500).json({"error": "There was an error updating the password"});
        }
        return res.status(200).json(password)
    });
});

router.delete('/password/:id', function(req, res, next){
    PasswordController.removePassword(req.params.id, function(err, message){
        if(err){
            return res.status(500).json(err);
        }
        return res.status(200).json(message)
    })
});

module.exports = router;
