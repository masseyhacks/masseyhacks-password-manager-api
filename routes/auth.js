var express = require('express');
var router = express.Router();

const permissions = require('../services/permissions');
const logging = require('../services/logging');

const UserController = require('../controllers/UserController');


/* GET home page. */
router.get('/', function(req, res, next) {

});

router.post('/login/', function(req, res){
    if(!req.body.email || !req.body.password){
        return res.status(400).json({"error": "Incomplete data"})
    }
    UserController.loginWithPassword(req.body.email, req.body.password, function (err, user){
        if (err){
            return res.status(401).json({"error": "Email or password incorrect"})
        }
        return res.status(200).json(user)
    })
});

router.post('/user/', function(req, res, next){
    if(!req.body.firstname || !req.body.lastname || !req.body.email || !req.body.password){
        return res.status(400).json({"error": "Incomplete data"})
    }

    UserController.createUser(req.body.firstname, req.body.lastname, req.body.email, req.body.password, function (err, message){
        if (err) {
            return res.status(500).json(err ? err : {error: err.error});
        }
        return res.status(200).json(message)
    });
});

router.get('/user/:id', function(req, res, next){
    console.log(req.body);

    res.json({"message": "OK"})
});

router.put('/user/:id', function(req, res, next){
    console.log(req.body);
    res.json({"message": "OK"})
});

router.delete('/user/:id', function(req, res, next){
    console.log(req.body);
    res.json({"message": "OK"})
});

module.exports = router;
