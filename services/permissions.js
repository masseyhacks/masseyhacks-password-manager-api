const TokenController = require('../controllers/TokenController');
const User = require('../models/User');
const Group = require('../models/Group');
const moment = require('moment');

function getToken(req) {
    var token = req['headers']['x-access-token'] ? req['headers']['x-access-token'] : false;

    if (!token) {
        token = req.body.token;
    }

    return token;
}

module.exports = {
    getToken : function (req)
    {
        return getToken(req);
    },
    isUser : function (req, res, next) {
        var token = getToken(req);
        TokenController.getTokenInfo(token, function(err, tokenInfo){
            if (err || !tokenInfo) {
                return res.status(403).send(err);
            }

            if(tokenInfo.expiresAt < moment().unix()){
                return res.status(403).send(err);
            }

            User.findOne({
                _id: tokenInfo.user
            }, function(err, user){
                if (err || !user) {
                    return res.status(403).send(err);
                }
                else{
                    req.userExecute = user;
                    req.userExecute['ip'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                    return next()
                }
            });


        });
    },
    isAuthorizedAccessObject: function (req, res, next){
        var token = getToken(req);
        TokenController.getTokenInfo(token, function(err, tokenInfo){
            if (err || !tokenInfo) {
                return res.status(403).send(err);
            }

            if(tokenInfo.expiresAt < moment().unix()){
                return res.status(403).send(err);
            }

            User.findOne({
                _id: tokenInfo.user
            }, function(err, user){
                if (err || !user) {
                    return res.status(403).send(err);
                }
                else{
                    Group.getByMember(user._id, function(err, groups){
                        if (err || !groups) {
                            return res.status(403).send(err);
                        }
                        // TODO: test authorization
                        req.userExecute = user;
                        req.userExecute['ip'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                        return next()
                    });

                }
            });


        });
    }
};