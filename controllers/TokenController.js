require('dotenv').config();

const moment = require('moment');
const hat = require('hat');

const Token = require('../models/Token');
var TokenController = {};

TokenController.genToken = function(userID, callback){
    let token = hat();
    let tokenCreated = moment();
    let tokenExpire = tokenCreated.add(process.env.TOKEN_DURATION, 'seconds');

    Token.create({
        token: token,
        user: userID,
        createdAt: tokenCreated,
        expiresAt: tokenExpire
    }, function(err){
        if(err){
            return callback({"error": "Unable to issue token"})
        }
        else{
            return callback(null, {"token": token, "expiry": tokenExpire.unix()})
        }
    })
};

TokenController.getTokenInfo = function(token, callback){
    Token.findOne({
        token: token
    }, function(err, tokenInfo){
        if(err){
            return callback(err)
        }
        else{
            return callback(null, tokenInfo)
        }
    })
};

module.exports = TokenController;