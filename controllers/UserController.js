const Group  = require('../models/Group');
const User = require('../models/User');
const bcrypt = require('bcrypt');

const TokenController = require('../controllers/TokenController');

var UserController = {};
UserController.createUser = function(firstname, lastname, email, password, callback){
    User.getByEmail(email, function (err, user){
        if(!err || user){
            return callback({"error": "A user with the given email already exists"})
        }
        bcrypt.hash(password, 10, function(err, hash) {
            User.create({
                firstName: firstname,
                lastName: lastname,
                email: email,
                password: hash
            }, function (err){
                console.log(err);
                return callback(null, {"code": 200, "message": "OK"})
            })
        });
    });
};

UserController.loginWithPassword = function(email, password, callback){
    User.findOne({email: email.toLowerCase()}, '+password', function (err, user){
        if(err || !user){
            return callback({"error": "Email or password incorrect"})
        }
        console.log(user);
        bcrypt.compare(password, user.password, function(err, res){
            if(res){
                TokenController.genToken(user._id, function(err, token){
                    if(err){
                        return callback({"error": "Unable to issue token"})
                    }
                    return callback(null, {"token": token, "user": User.filterSensitive(user)})
                })

            }
            else{
                return callback({"error":"Email or password incorrect"})
            }
        })
    })
};

UserController.getGrants = function (userID, callback){
    Group.getByMember(userID, function (err, groups){

    })
};

module.exports = UserController;