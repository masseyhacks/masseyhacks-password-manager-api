const Group  = require('../models/Group');
const User = require('../models/User');

var GroupController = {};

GroupController.createGroup = function(name,description, callback){
    Group.getByName(name, function(err, group){
        if(!err || group){
            return callback({error: "The group with the given name already exists"})
        }

        let groupSchema = {
            name: name
        };
        if(description){
            groupSchema.description = description;
        }
        Group.create(groupSchema, function(err, group){
            if(err || !group){
                return callback(err)
            }
            return callback(null, {code: 200, message: "Group created", groupID: group._id})
        });
    });
};

GroupController.addMember = function(id, groupID, callback){
    Group.findOneAndUpdate(
        {
        _id: groupID
        },
        {
            $push: {
                members: id
            }
        }, {new: true}, function (err, group){
            if(err || !group){
                return callback(err)
            }
            return callback(null, group)
        })
};

module.exports = GroupController;